cd src

gcc -std=c99 -s -DLINUX -o maskprocessor mp.c

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "GCC Compile... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf maskprocessor /opt/ANDRAX/bin

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
